<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    protected $table = 'menu';

    use HasFactory, SoftDeletes;

    protected $fillable = [
        'naam',
        'pannenkoek',
        'drinken',
        'achttien',
        'strippen',
    ];
}
