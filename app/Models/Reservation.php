<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $table = 'reservations';
    public $incrementing = false;
//    protected $primaryKey = 'id';
//    protected $keyType = 'string';

    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'email',
        'adults',
        'children',
    ];
}
