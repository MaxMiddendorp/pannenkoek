<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    use HasFactory;

    protected $fillable = [
        'tafel',
        'order',
        'message',
        'user_id',
        'order_status',
    ];

    const ORDER_STATUS = [
        0 => 'Besteld',
        1 => 'Wordt bereid',
        2 => 'Serveren',
        3 => 'Klaar'
    ];
}
