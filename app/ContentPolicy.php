<?php
namespace App;
use Spatie\Csp\Directive;
use Spatie\Csp\Policies\Basic;
class ContentPolicy extends Basic
{
    public function configure()
    {
        parent::configure();

        $this->addDirective(Directive::DEFAULT, 'fonts.googleapis.com');
        $this->addDirective(Directive::DEFAULT, 'cdnjs.cloudflare.com');
        $this->addDirective(Directive::DEFAULT, 'unpkg.com');
    }
}
