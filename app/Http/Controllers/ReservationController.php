<?php

namespace App\Http\Controllers;

use App\Models\Reservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class ReservationController extends Controller
{
    public function index()
    {
//        $reservations = Reservation::all();
        $reservations = DB::table('reservations')->orderBy('status', 'desc')->get();

        return view('reservations.index', [
            'reservations' => $reservations,
        ]);
    }

    public function create()
    {
        return view('reservations.create');
    }

    public function save(Request $request)
    {
        $request->request->add(['id' => strtoupper(Str::random(6))]);
        $request->validate([
            'name' => 'required|string|max:50',
            'email' => 'required|email|max:100',
            'adults' => 'required|integer|max:20',
            'children' => 'required|integer|max:20',
        ]);
        $reservation = new Reservation($request->all());
        $reservation->save();
        $array = $request->all();
        app('App\Http\Controllers\ReservationController')->mail($array);
        return redirect()->route('reservations.index')->with('order', $request->id);
    }

    public function mail($array)
    {
        Mail::send('mail.confirmation', ['array' => $array], function ($message) use ($array) {
            $message->to($array['email'], $array['name'])->subject
            ('Reservering pannenkoekenrestaurant');
        });
    }

    public function show()
    {
        return view('reservations.show');
    }

    public function status(Reservation $reservation)
    {
        $reservation = Reservation::find($reservation->id);
        if ($reservation->status == 0) {
            $reservation->status = $reservation->status + 1;
        } elseif ($reservation->status == 1) {
            $reservation->status = $reservation->status - 1;
        }
        $reservation->update();
        return back();
    }

    public function edit(Reservation $reservation)
    {
        return view('reservations.edit', [
            'reservation' => $reservation
        ]);
    }

    public function editsave(Request $request, Reservation $reservation)
    {
        $reservation->name = $request->input('name');
        $reservation->email = $request->input('email');
        $reservation->adults = $request->input('adults');
        $reservation->children = $request->input('children');

        $reservation->update();

        return redirect()->route('reservations.index')->with('success', 'Succesvol opgeslagen');
    }

    public function delete(Reservation $reservation)
    {
        $delreservation = Reservation::find($reservation->id);
        $delreservation->delete();
        return back();
    }
}
