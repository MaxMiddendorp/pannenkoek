<?php

namespace App\Http\Controllers;

use App\Models\Reservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    public function index()
    {
        return view('home.index');
    }

    public function save(Request $request)
    {
//        $request->id = Str::random(6);
//dd($request->id);
        $request->request->add(['id' => strtoupper(Str::random(6))]);
//        $request->request->add(['status' => 0]);
        $request->validate([
            'name' => 'required|string|max:50',
            'email' => 'required|email|max:100',
            'adults' => 'required|integer|max:20',
            'children' => 'required|integer|max:20',
        ]);
        $reservation = new Reservation($request->all());
        $reservation->save();
//        return redirect()->route('home.done')->with(['order' => $request->id]);
//        Notification::send($reservation, new ReservationConfirmation($reservation));
        $array = $request->all();
//        dd($array['email']);
        app('App\Http\Controllers\HomeController')->mail($array);
        return redirect()->to('/done')->with('order', $request->id);

    }

    public function mail($array)
    {
        Mail::send('mail.confirmation', ['array' => $array], function ($message) use ($array) {
            $message->to($array['email'], $array['name'])->subject
            ('Reservering pannenkoekenrestaurant');
        });
    }

    public function showmail(){
        return view('mail.confirmation');
    }

    public function done()
    {
        return view('home.done');
    }
}
