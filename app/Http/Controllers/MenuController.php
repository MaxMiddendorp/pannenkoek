<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class MenuController extends Controller
{
    public function index()
    {
        $menus = Menu::all();
        return view('menu.index', [
            'menus' => $menus,
        ]);
    }

    public function create()
    {
        return view('menu.create');
    }

    public function save(Request $request)
    {
        $request->validate([
            'naam' => 'required|string|max:255',
            'pannenkoek' => 'integer',
            'drinken' => 'integer',
            'achttien' => 'integer',
            'strippen' => 'max:10',
        ]);
        $menu = new Menu($request->all());
        $menu->save();
        return redirect()->route('menu.index')->with('success', 'Succesvol opgeslagen');
    }

    public function edit(menu $menu)
    {
        return view('menu.edit', [
            'menu' => $menu
        ]);
    }

    public function editsave(Request $request, menu $menu)
    {
        $menu->naam = $request->input('naam');
        if($request->input('pannenkoek') == null)
            $menu->pannenkoek = 0;
        elseif ($request->input('pannenkoek') == 1){
        $menu->pannenkoek = 1;
        }
        if($request->input('drinken') == null)
            $menu->drinken = 0;
        elseif ($request->input('drinken') == 1){
            $menu->drinken = 1;
        }
        if($request->input('achttien') == null)
            $menu->achttien = 0;
        elseif ($request->input('achttien') == 1){
            $menu->achttien = 1;
        }
        $menu->strippen = $request->input('strippen');
        $menu->update();
        return redirect()->route('menu.index')->with('success', 'Succesvol opgeslagen');
    }

    public function delete(menu $menu)
    {
        $delmenu = Menu::find($menu->id);
        $delmenu->delete();
        return back();
    }


    public function show()
    {
        return view('users.show');
    }
}
