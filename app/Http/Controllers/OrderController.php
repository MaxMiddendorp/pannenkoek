<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\Order;
use App\Models\Ordersitems;
use App\Models\Table;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::all();
        return view('orders.index', [
            'orders' => $orders,
        ]);
    }

    public function list()
    {
        $orders = DB::table('orders')->where('order_status', '!=', 3)->orderBy('order_status', 'desc')->limit(30)->get();
        $menuitems = Menu::all();
        return view('orders.list', [
            'orders' => $orders,
            'menuitems' => $menuitems,
        ]);
    }

    public function listfull()
    {
        $orders = DB::table('orders')->where('order_status', '!=', 3)->orderBy('order_status', 'desc')->limit(30)->get();
        $menuitems = Menu::all();
        return view('orders.listfull', [
            'orders' => $orders,
            'menuitems' => $menuitems,
        ]);
    }

    public function full()
    {
        $orders = DB::table('orders')->where('order_status', '!=', 3)->orderBy('order_status', 'desc')->limit(30)->get();
        $menuitems = Menu::all();
        return view('orders.full', [
            'orders' => $orders,
            'menuitems' => $menuitems,
        ]);
    }

    public function create()
    {
//        $menuitems = Menu::all();
        $menu_pancakes = Menu::where('pannenkoek', 1)->where('achttien', 0)->get();
        $menu_pancakes_18 = Menu::where('pannenkoek', 1)->where('achttien', 1)->get();
        $menu_drinks = Menu::where('drinken', 1)->where('achttien', 0)->get();
        $menu_drinks_18 = Menu::where('drinken', 1)->where('achttien', 1)->get();
        $tables = Table::all();
        return view('orders.create', [
            'menu_pancakes' => $menu_pancakes,
            'menu_pancakes_18' => $menu_pancakes_18,
            'menu_drinks' => $menu_drinks,
            'menu_drinks_18' => $menu_drinks_18,
            'tables' => $tables,
        ]);
//        return view('orders.create');
    }

    public function save(Request $request)
    {
        $request['user_id'] = Auth::id();
        $request['order'] = serialize($request['order']);
        $request['order_status'] = 0;
        $request->validate([
            'tafel' => 'required',
            'order' => 'required',
            'message' => 'nullable',
            'user_id' => 'required',
            'order_status' => 'integer',
        ]);
        $order = new Order($request->all());
        $order->save();
        return redirect()->route('orders.index')->with('success', 'Succesvol opgeslagen');
    }


    public function show()
    {
        return view('orders.show');
    }

    public function edit(order $order)
    {
        $menu_pancakes = Menu::where('pannenkoek', 1)->where('achttien', 0)->get();
        $menu_pancakes_18 = Menu::where('pannenkoek', 1)->where('achttien', 1)->get();
        $menu_drinks = Menu::where('drinken', 1)->where('achttien', 0)->get();
        $menu_drinks_18 = Menu::where('drinken', 1)->where('achttien', 1)->get();
        $tables = Table::all();
        $bestelling = unserialize($order->order);
        return view('orders.edit', [
            'order' => $order,
            'bestelling' => $bestelling,
            'menu_pancakes' => $menu_pancakes,
            'menu_pancakes_18' => $menu_pancakes_18,
            'menu_drinks' => $menu_drinks,
            'menu_drinks_18' => $menu_drinks_18,
            'tables' => $tables,
        ]);
//        return view('orders.create');
    }


    public function editsave(Request $request, Order $order)
    {
        $request['user_id'] = Auth::id();
        $request->validate([
            'tafel' => 'required',
//            'order' => 'required',
            'message' => 'nullable',
            'user_id' => 'required',
            'order_status' => 'integer',
        ]);
        $order->tafel = $request['tafel'];
        $order->message = $request['message'];
        $order->user_id = $request['user_id'];
        $order->update();
        return redirect()->route('orders.index')->with('success', 'Succesvol opgeslagen');
    }

    public function status(Order $order)
    {
        $status = Order::find($order->id);
        if ($order->order_status < 3) {
            $order->order_status = $order->order_status + 1;
        }
        $order->update();
        return back();
    }

    public function delete(Order $order)
    {
        $delorder = Order::find($order->id);
        $delorder->delete();
        return back();
    }
}
