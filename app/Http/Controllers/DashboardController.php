<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Reservation;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $reservations = Reservation::all()->count();
        $status0 = Order::where('order_status', 0)->count();
        $status1 = Order::where('order_status', 1)->count();
        $status2 = Order::where('order_status', 2)->count();
        $status3 = Order::where('order_status', 3)->count();
        return view('dashboard', [
            'reservations' => $reservations,
            'status0' => $status0,
            'status1' => $status1,
            'status2' => $status2,
            'status3' => $status3,
        ]);
    }
}
