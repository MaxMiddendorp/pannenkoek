<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
//        dd(Auth::user());
        $users = User::all();
        return view('users.index', [
            'users' => $users,
        ]);
//        return view('users.index');
    }

    public function create()
    {
//        $menuitems = Menu::all();
//        return view('orders.create', [
//            'menuitems' => $menuitems,
//        ]);
        return view('users.create');
    }

    public function save(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|confirmed|min:6',
        ]);
        $user = new User($request->all());
        $user->password = Hash::make($request->password);
        $user->save();
        if ($user) {
            return redirect()->route('users.index')->with('success', 'Succesvol opgeslagen');
        } else {
            return redirect()->route('users.index')->with('error', 'Er is iets misgegaan');
        }

    }

    public function edit(user $user)
    {
        return view('users.edit', [
            'user' => $user
        ]);
    }

    public function editsave(Request $request, user $user)
    {
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        if ($request['password'] != null) {
            $user->password = Hash::make($request->input('password'));
        }
        $user->update();
        if ($user) {
            return redirect()->route('users.index')->with('success', 'Succesvol opgeslagen');
        }
    }

    public function delete(User $user)
    {
        $deluser = User::find($user->id);
        $deluser->delete();
        return back();
    }



    public function show()
    {
        return view('users.show');
    }
}
