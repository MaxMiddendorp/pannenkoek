<?php

namespace App\Http\Controllers;

use App\Models\Table;
use Illuminate\Http\Request;

class TableController extends Controller
{
    public function index()
    {
        $tables = Table::all();
        return view('tables.index', [
            'tables' => $tables,
        ]);
    }

    public function create()
    {
        return view('tables.create');
    }

    public function save(Request $request)
    {
        $request->validate([
            'tablenumber' => 'required',
        ]);
        $tables = new Table($request->all());
        $tables->save();
        return redirect()->route('tables.index');
    }

    public function status(Table $table)
    {
        $table = Table::find($table->id);
        if ($table->status == 0) {
            $table->status = $table->status + 1;
        } elseif ($table->status == 1) {
            $table->status = $table->status - 1;
        }
        $table->update();
        return back();
    }
}
