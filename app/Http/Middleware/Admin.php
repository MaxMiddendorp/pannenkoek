<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\TrimStrings as Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Admin extends Middleware
{
    public function handle($request, Closure $next, $right=null)
    {
        $user = $request->user();
//dd($user);
        if ($user && $user->is_admin) {
            return $next($request);
        }
        $request_url = $request->path();
//        session()->put('login_refferrer', $request_url);
//        TODO: show an error message with toastr
        return back()->withErrors('critical', 'You are not allowed to enter this page');
    }
}
