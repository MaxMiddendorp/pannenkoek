<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Je wachtwoord is gereset',
    'sent' => 'Wachtwoord herstel link is successvol verstuurd',
    'throttled' => 'Wacht even tot je het opnieuw probeert',
    'token' => 'De wachtwoord herstel token is ongeldig',
    'user' => "Er bestaat geen gebruiker met dit email adres",

];
