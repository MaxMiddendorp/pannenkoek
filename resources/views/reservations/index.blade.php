<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Reserveringen') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <a href="{{ route('reservations.create') }}"><i
                            class="fa-solid fa-plus fill-gray-500 p-3 object-right	"></i></a>
                    <div class="overflow-x-auto">
                        <table class="w-full text-sm text-left text-gray-500">
                            <thead class="text-xs text-gray-700 uppercase bg-gray-50">
                            <tr>
                                <th scope="col" class="px-6 py-3">
                                    #
                                </th>
                                <th class="px-6 py-3">
                                    Naam
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Email
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    12+
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    12-
                                </th>
                                {{--                            <th scope="col" class="px-6 py-3">--}}
                                {{--                                Totaal--}}
                                {{--                            </th>--}}
                                <th scope="col" class="px-6 py-3">
                                    Status
                                </th>
                                <th scope="col" class="px-6 py-3">

                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($reservations as $reservation)
                                <tr class="bg-white border-b">
                                    <th scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                                        {{ $reservation->id }}
                                    </th>
                                    <td class="px-6 py-4">
                                        {{ $reservation->name }}
                                    </td>
                                    <td class="px-6 py-4">
                                        {{ $reservation->email }}
                                    </td>
                                    <td class="px-6 py-4">
                                        {{ $reservation->adults }}
                                    </td>
                                    <td class="px-6 py-4">
                                        {{ $reservation->children }}
                                    </td>
                                    {{--                                <td class="px-6 py-4">--}}
                                    {{--                                    <div class="hidden">{{ $total = ($reservation->adults * 7.50) + ($reservation->children * 5.00) }}</div>--}}
                                    {{--                                    €{{ $total }},---}}
                                    {{--                                </td>--}}
                                    <td class="px-6 py-4">
                                        @if($reservation->status == 0)
                                            <span class="mr-3"><span
                                                    class="whitespace-nowrap text-uppercase inline-flex items-center px-2.5 py-1 text-sm rounded-full bg-red-400 bg-opacity-10 text-red-900"><span
                                                        class="relative flex mr-1.5 w-2.5 h-2.5"><span
                                                            class="relative inline-flex w-2.5 h-2.5 rounded-full bg-red-400"></span></span>Gereserveerd</span></span>
                                        @elseif($reservation->status == 1)
                                            <span class="mr-3"><span
                                                    class="whitespace-nowrap text-uppercase inline-flex items-center px-2.5 py-1 text-sm rounded-full bg-lime-400 bg-opacity-10 text-lime-900"><span
                                                        class="relative flex mr-1.5 w-2.5 h-2.5"><span
                                                            class="relative inline-flex w-2.5 h-2.5 rounded-full bg-lime-400"></span></span>Aanwezig</span></span>
                                        @else
                                            Error
                                        @endif
                                    </td>
                                    <td class="px-6 py-4 text-right flex flex-row">
                                        <form method="POST"
                                              action="{{ route('reservations.status', [ 'reservation' => $reservation->id ]) }}">
                                            @csrf

                                            <a href="{{ route('reservations.status', [ 'reservation' => $reservation->id ]) }}"
                                               onclick="event.preventDefault();
                                                this.closest('form').submit();">
                                                @if($reservation->status == 0)
                                                    <i class="fa-solid fa-forward-fast"></i>
                                                @elseif($reservation->status == 1)
                                                    <i class="fa-solid fa-backward-fast"></i>
                                                @endif
                                            </a>
                                        </form>

                                        <a href="{{ route('reservations.edit', [ 'reservation' => $reservation->id ]) }}"><i
                                                class="fa-solid fa-pen px-2"></i></a>

                                        <form method="POST"
                                              action="{{ route('reservations.delete', [ 'reservation' => $reservation->id ]) }}">
                                            @csrf

                                            <a href="{{ route('reservations.delete', [ 'reservation' => $reservation->id ]) }}"
                                               onclick="event.preventDefault();
                                                this.closest('form').submit();">
                                                <i class="fa-solid fa-trash"></i>
                                            </a>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(Session::get('order') !== null)
        <div id="toast-success"
             class="flex items-center w-full max-w-xs p-4 m-4 mt-auto ml-auto text-gray-500 bg-white rounded-lg shadow dark:text-gray-400 dark:bg-gray-800"
             role="alert">
            <div
                class="inline-flex items-center justify-center flex-shrink-0 w-8 h-8 text-green-500 bg-green-100 rounded-lg dark:bg-green-800 dark:text-green-200">
                <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                          d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                          clip-rule="evenodd"></path>
                </svg>
            </div>
            <div class="ml-3 text-sm font-normal">{{ Session::get('order') }}</div>
            <button type="button"
                    class="ml-auto -mx-1.5 -my-1.5 bg-white text-gray-400 hover:text-gray-900 rounded-lg focus:ring-2 focus:ring-gray-300 p-1.5 hover:bg-gray-100 inline-flex h-8 w-8 dark:text-gray-500 dark:hover:text-white dark:bg-gray-800 dark:hover:bg-gray-700"
                    data-dismiss-target="#toast-success" aria-label="Close">
                <span class="sr-only">Close</span>
                <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                          d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                          clip-rule="evenodd"></path>
                </svg>
            </button>
        </div>
    @endif

</x-app-layout>
