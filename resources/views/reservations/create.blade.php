<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Reservering maken') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-32">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <form method="POST" action="{{ route('reservations.save') }}">
                    @csrf
                    <div class="p-6 bg-white border-b border-gray-200">
                        <label class="block">
                            <span class="text-gray-700">Naam</span>
                            <input type="text" name="name"
                                   class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                                   placeholder="John Doe" value="{{old('name')}}" required>
                        </label>
                    </div>
                    <div class="p-6 bg-white border-b border-gray-200">
                        <label class="block">
                            <span class="text-gray-700">Email</span>
                            <input type="email" name="email"
                                   class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                                   placeholder="john.doe@example.com" value="{{old('email')}}" required>
                        </label>
                    </div>
                    <div class="p-6 bg-white border-b border-gray-200">
                        <label class="block">
                            <span class="text-gray-700">Aantal volwassenen €7,50</span>
                            <input type="number" name="adults"
                                   class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                                   placeholder="0" value="{{old('adults')}}" required>
                        </label>
                    </div>
                    <div class="p-6 bg-white border-b border-gray-200">
                        <label class="block">
                            <span class="text-gray-700">Aantal kinderen (tot 12) €5,00</span>
                            <input type="number" name="children"
                                   class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                                   placeholder="0" value="{{old('children')}}" required>
                        </label>
                    </div>
                    <div class="p-6 bg-white border-b border-gray-200">
                        <div class="block">
                            <label for="remember_me" class="inline-flex items-center cursor-default">
                                <input id="remember_me" type="checkbox" required
                                       class="rounded border-gray-300 cursor-pointer text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                                       name="remember"></label>
                            <span class="ml-2 text-sm text-gray-600 cursor-default">Ga akkoord met de </span><a
                                class="text-blue-700 text-sm pl-1 cursor-pointer" data-modal-toggle="terms-modal">
                                Algemene Voorwaarden</a>
                            <div id="terms-modal" tabindex="-1"
                                 class="fixed top-0 left-0 right-0 z-50 hidden w-full overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full">
                                <div class="relative w-full h-full max-w-lg p-4 md:h-auto">
                                    <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
                                        <div
                                            class="flex items-center justify-between p-5 border-b rounded-t dark:border-gray-600">
                                            <h3 class="text-xl font-medium text-gray-900 dark:text-white">
                                                Algemene Voorwaarden
                                            </h3>
                                            <button type="button"
                                                    class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white"
                                                    data-modal-toggle="terms-modal">
                                                <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd"
                                                          d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                                          clip-rule="evenodd"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="p-6 space-y-6">
                                            <p class="text-base leading-relaxed text-gray-500 dark:text-gray-400">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                Pellentesque volutpat purus magna, sed interdum ante fermentum in.
                                                Pellentesque habitant morbi tristique senectus et netus et malesuada
                                                fames ac turpis egestas. Ut eget accumsan magna. Proin in magna quis
                                                turpis convallis finibus ac aliquam lorem. Maecenas interdum quam
                                                sit amet metus aliquet, eget volutpat ipsum sagittis. In tincidunt
                                                et lectus ut ultrices. Mauris vitae vulputate quam. Phasellus
                                                elementum tellus elit, ac porta erat ultricies a. Nunc tincidunt
                                                viverra convallis. Etiam cursus orci accumsan interdum viverra.
                                                Etiam vel turpis lectus.

                                                Proin eu justo metus. Aenean eu magna quis dui malesuada
                                                sollicitudin ut sed turpis. Praesent non augue molestie libero
                                                imperdiet hendrerit. Aliquam vitae tellus sem. Duis ut felis dolor.
                                                Praesent fringilla ex purus, id vulputate dolor tincidunt sit amet.
                                                Fusce diam urna, fermentum a urna eget, hendrerit convallis quam.
                                                Sed iaculis non purus id eleifend. Curabitur ut massa nec enim
                                                auctor cursus. In dignissim leo enim, eget ultricies dui posuere
                                                vel. Mauris sodales dictum cursus. Sed sit amet lectus magna. Sed
                                                lacinia felis quam, in tincidunt libero suscipit ut. In eget magna
                                                sollicitudin, cursus lorem eget, dictum neque. Vestibulum non
                                                rhoncus nunc.
                                            </p>
                                        </div>
                                        <div
                                            class="flex items-center p-6 space-x-2 border-t border-gray-200 rounded-b dark:border-gray-600">
                                            <button data-modal-toggle="terms-modal" type="button"
                                                    class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                                Sluit
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="p-6 bg-white border-b border-gray-200">
                        <div class="block">
                            <button type="submit"
                                    class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">
                                Maak reservering aan
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
