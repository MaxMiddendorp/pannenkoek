<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Reservering bewerken') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-32">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <form method="POST" action="{{ route('reservations.editsave', [ 'reservation' => $reservation ]) }}">
                    @csrf
                    <div class="p-6 bg-white border-b border-gray-200">
                        <label class="block">
                            <span class="text-gray-700">Naam</span>
                            <input type="text" name="name"
                                   class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                                   placeholder="John Doe" value="{{$reservation->name}}" required>
                        </label>
                    </div>
                    <div class="p-6 bg-white border-b border-gray-200">
                        <label class="block">
                            <span class="text-gray-700">Email</span>
                            <input type="email" name="email"
                                   class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                                   placeholder="john.doe@example.com" value="{{$reservation->email}}" required>
                        </label>
                    </div>
                    <div class="p-6 bg-white border-b border-gray-200">
                        <label class="block">
                            <span class="text-gray-700">Aantal volwassenen €7,50</span>
                            <input type="number" name="adults"
                                   class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                                   placeholder="0" value="{{$reservation->adults}}" required>
                        </label>
                    </div>
                    <div class="p-6 bg-white border-b border-gray-200">
                        <label class="block">
                            <span class="text-gray-700">Aantal kinderen (tot 12) €5,00</span>
                            <input type="number" name="children"
                                   class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                                   placeholder="0" value="{{$reservation->children}}" required>
                        </label>
                    </div>
                    <div class="p-6 bg-white border-b border-gray-200">
                        <div class="block">
                            <button type="submit"
                                    class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">
                                Reservering bewerken
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
