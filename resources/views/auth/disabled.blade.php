<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <img src="{{ asset('/img/hedera.svg') }}" class="w-20 h-20 fill-current text-gray-500"/>
            </a>
        </x-slot>
        <style>
            a {
                outline: none;
                text-decoration: none;
                padding: 2px 1px 0;
            }

            a:link {
                color: #0000EE;
            }

            a:visited {
                color: #551A8B;
            }
        </style>
        Deze pagina is (tijdelijk) uitgeschakeld. <br> Moet u toch bij deze pagina zijn? Neem dan contact op met de
        administrator. Mail: <a class="" href="mailto:mail@explorers.ovh">mail@explorers.ovh</a><br>Excuses voor het
        ongemak.
    </x-auth-card>
</x-guest-layout>
