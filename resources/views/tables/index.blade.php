<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Tafels') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <a href="{{ route('tables.create') }}"><i
                            class="fa-solid fa-plus fill-gray-500 p-3 object-right	"></i></a>
                    <div class="overflow-x-auto">
                        <table class="w-full text-sm text-left text-gray-500">
                            <thead class="text-xs text-gray-700 uppercase bg-gray-50">
                            <tr>
                                <th class="px-6 py-3">
                                    #
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Status
                                </th>
                                <th scope="col" class="px-6 py-3">

                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tables as $table)
                                <tr class="bg-white border-b">
                                    <td class="px-6 py-4">
                                        {{ $table->tablenumber }}
                                    </td>
                                    <td class="px-6 py-4">
                                        @if($table->status == 0)
                                            <span class="mr-3"><span
                                                    class="whitespace-nowrap text-uppercase inline-flex items-center px-2.5 py-1 text-sm rounded-full bg-red-400 bg-opacity-10 text-red-900"><span
                                                        class="relative flex mr-1.5 w-2.5 h-2.5"><span
                                                            class="relative inline-flex w-2.5 h-2.5 rounded-full bg-red-400"></span></span>Niet-actief</span></span>
                                        @elseif($table->status == 1)
                                            <span class="mr-3"><span
                                                    class="whitespace-nowrap text-uppercase inline-flex items-center px-2.5 py-1 text-sm rounded-full bg-lime-400 bg-opacity-10 text-lime-900"><span
                                                        class="relative flex mr-1.5 w-2.5 h-2.5"><span
                                                            class="relative inline-flex w-2.5 h-2.5 rounded-full bg-lime-400"></span></span>Actief</span></span>
                                        @else
                                            Error
                                        @endif
                                    </td>
                                    <td class="px-6 py-4 text-right flex flex-row">
                                        <form method="POST"
                                              action="{{ route('tables.status', [ 'table' => $table->id ]) }}">
                                            @csrf

                                            <a href="{{ route('tables.status', [ 'table' => $table->id ]) }}"
                                               onclick="event.preventDefault();
                                                this.closest('form').submit();">
                                                <i class="fa-solid fa-forward-fast"></i>
                                            </a>
                                        </form>

                                    {{--                                    @if(\Illuminate\Support\Facades\Auth::user()->is_admin)--}}
                                    {{--                                        <a><i class="fa-solid fa-pen ml-4"></i></a>--}}
                                    {{--                                    @endif--}}
                                    {{--                                </td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
