<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description"
          content="Reserveringssysteem pannenkoekenrestaurant Scouting Hedera Amstelveen 4 juli 2022">

    <meta property="og:title" content="Pannenkoekenrestaurant Scouting Hedera Amstelveen"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="{{ URL::to('/') }}"/>
    <meta property="og:image" content="{{ URL::to('/') }}/img/hedera.png"/>
    <meta property="og:description"
          content="Pannenkoekenrestaurant Explorers Scouting Hedera Amstelveen - 4 juli 2022 17:00 ~ 21:00"/>
    <meta property="og:image:width" content="300">
    <meta property="og:image:height" content="300">

    {{--    <meta name="twitter:card" content="summary_large_image">--}}
    {{--    <meta name="twitter:url" content="{{ env('app_url') }}" />--}}
    {{--    <meta name="twitter:title" content="Pannenkoekenrestaurant Scouting Hedera Amstelveen" />--}}
    {{--    <meta name="twitter:image" content="{{ env('app_url') }}/img/hedera.jpg" />--}}
    {{--    <meta name="twitter:description" content="Pannenkoekenrestaurant Explorers Scouting Hedera Amstelveen - 4 juli 2022 17:00 ~ 21:00" />--}}

    <meta name="twitter:card" content="summary_large_image">
    {{--    <meta property="twitter:domain" content="explorers.ovh">--}}
    <meta property="twitter:url" content="{{ URL::to('/') }}">
    <meta name="twitter:title" content="Pannenkoekenrestaurant Scouting Hedera Amstelveen">
    <meta name="twitter:description"
          content="Pannenkoekenrestaurant Explorers Scouting Hedera Amstelveen - 4 juli 2022 17:00 ~ 21:00">
    <meta name="twitter:image" content="{{ URL::to('/') }}/img/hedera.png">

    <link rel="canonical" href="{{ URL::to('/') }}">

    <title>Pannenkoekenrestaurant Scouting Hedera Amstelveen</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
          integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
            integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>
<body class="font-sans antialiased">
<div class="min-h-screen bg-gray-100">
    <header class="bg-white shadow">
        <div class="max-w-7xl mx-auto py-4 px-4 sm:px-6 lg:px-8 flex flex-row justify-center">
            <img src="{{ asset('img/Coop_Logo.svg') }}" alt="Logo Coop Supermarkten" class="h-16"/>
            <img src="{{ asset('img/hedera.svg') }}" alt="Scouting Hedera Logo" class="h-16 w-16"/>
            <img src="{{ asset('img/ahvdl.png') }}" alt="Logo AH van der Linden" class="h-16"/>
        </div>
        <div class="max-w-7xl mx-auto py-4 px-4 sm:px-6 lg:px-8 flex flex-row justify-center">
            <h1 class="text-lg align-middle my-auto mx-4">Pannenkoeken restaurant Scouting Hedera</h1>
        </div>
    </header>

    <main>
        <div class="py-12">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-32">
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="p-6 bg-white border-b border-gray-200">
                        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                            {{ __('Reservering maken') }}
                        </h2>
                    </div>
                    <form method="POST" action="{{ route('home.save') }}">
                        @csrf
                        <div class="p-6 bg-white border-b border-gray-200">
                            <label class="block">
                                <span class="text-gray-700">Naam</span>
                                <input type="text" name="name"
                                       class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                                       placeholder="John Doe" value="{{old('name')}}" required>
                            </label>
                        </div>
                        <div class="p-6 bg-white border-b border-gray-200">
                            <label class="block">
                                <span class="text-gray-700">Email</span>
                                <input type="email" name="email"
                                       class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                                       placeholder="john.doe@example.com" value="{{old('email')}}" required>
                            </label>
                        </div>
                        <div class="p-6 bg-white border-b border-gray-200">
                            <label class="block">
                                <span class="text-gray-700">Aantal volwassenen €7,50</span>
                                <input type="number" name="adults"
                                       class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                                       placeholder="0" value="{{old('adults')}}" required>
                            </label>
                        </div>
                        <div class="p-6 bg-white border-b border-gray-200">
                            <label class="block">
                                <span class="text-gray-700">Aantal kinderen (tot 12) €5,00</span>
                                <input type="number" name="children"
                                       class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                                       placeholder="0" value="{{old('children')}}" required>
                            </label>
                        </div>
                        <div class="p-6 bg-white border-b border-gray-200">
                            <div class="block">
                                <label for="remember_me" class="inline-flex items-center cursor-default">
                                    <input id="remember_me" type="checkbox" required
                                           class="rounded border-gray-300 cursor-pointer text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                                           name="remember"></label>
                                <span class="ml-2 text-sm text-gray-600 cursor-default">Ga akkoord met de </span><a
                                    class="text-blue-700 text-sm pl-1 cursor-pointer" data-modal-toggle="terms-modal">
                                    Algemene Voorwaarden</a>

                                <div id="terms-modal" tabindex="-1"
                                     class="fixed top-0 left-0 right-0 z-50 hidden w-full overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full">
                                    <div class="relative w-full h-full max-w-lg p-4 md:h-auto">
                                        <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
                                            <div
                                                class="flex items-center justify-between p-5 border-b rounded-t dark:border-gray-600">
                                                <h3 class="text-xl font-medium text-gray-900 dark:text-white">
                                                    Algemene Voorwaarden
                                                </h3>
                                                <button type="button"
                                                        class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white"
                                                        data-modal-toggle="terms-modal">
                                                    <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd"
                                                              d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                                              clip-rule="evenodd"></path>
                                                    </svg>
                                                </button>
                                            </div>
                                            <div class="p-6 space-y-6">
                                                <p class="text-base leading-relaxed text-gray-500 dark:text-gray-400">
                                                    Door te reserveren krijgt u een korting van €1 per persoon:<br>
                                                    Voor volwassenen €7,50<br>
                                                    Voor kinderen (tot 12) €5,00<br>
                                                    <br>
                                                    Zonder reserverkorting is dit €8,50 en €6,00.<br>
                                                    <br>
                                                    Bovenstaande prijs is alleen voor pannenkoeken. Per bestelling gaan
                                                    wij uit van 1 pannenkoek per persoon waar voor betaald is. Het aantal
                                                    bestellingen is gelimiteerd aan hoeveel u lust!
                                                    <br><br>
                                                    Voor drankjes kunt u een strippenkaart van 10 strippen kopen voor
                                                    €5,00. Drankjes kosten 1 of 2 strippen afhankelijk van uw keuze. Ook
                                                    voor sommige pannenkoeken vragen wij extra strippen. Dit staat op de
                                                    menukaart vermeld.
                                                    <br><br>
                                                    Wij houden ons aan de alcohol regels die gelden in nederland, indien
                                                    iemand geen 18 jaar of ouder is dan serveren wij geen alcohol aan
                                                    deze klant
                                                </p>
                                            </div>
                                            <div
                                                class="flex items-center p-6 space-x-2 border-t border-gray-200 rounded-b dark:border-gray-600">
                                                <button data-modal-toggle="terms-modal" type="button"
                                                        class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                                    Sluit
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="p-6 bg-white border-b border-gray-200">
                            <div class="block">
                                <button type="submit"
                                        class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">
                                    Reserveer
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
</div>
<script src="https://unpkg.com/flowbite@1.4.3/dist/flowbite.js"></script>
</body>
</html>
