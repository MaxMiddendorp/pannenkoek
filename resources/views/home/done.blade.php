<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
          integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
            integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>
<body class="font-sans antialiased">
<div class="min-h-screen bg-gray-100">
    <header class="bg-white shadow">
        <div class="max-w-7xl mx-auto py-4 px-4 sm:px-6 lg:px-8 flex flex-row">
            <img src="{{ asset('img/hedera.svg') }}" alt="Scouting Hedera Logo" class="h-16 w-16"/>
            <p class="text-lg align-middle my-auto mx-4">Pannenkoeken restaurant Scouting Hedera</p>
        </div>
    </header>
    <main>
        <div class="py-12">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-32">
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg mx-auto">
                    <div class="p-6 bg-white border-b border-gray-200">
                        <h2 class="font-semibold text-xl text-gray-800 leading-tight text-center">
                            Uw reservering is opgeslagen!
                        </h2>
                    </div>
                    <div class="bg-white border-b border-gray-200">
                        <img src="{{ asset('img/thumb.gif') }}" alt="Thumbs up GIF" class="block ml-auto mr-auto"/>
                    </div>
                    <div class="p-6 bg-white border-b border-gray-200">
                        <h3 class="font-semibold text-md text-gray-800 leading-tight text-center">
                            Uw reserveringsnummer
                            is: @if(Session::get('order') !== null)
                                {{ Session::get('order') }}
                            @else
                                Er is een fout
                                opgetreden, check uw email!
                            @endif
                        </h3>
                        <h3 class="font-semibold text-sm text-gray-800 leading-tight text-center">
                            Een orderbevestiging staat ook in de mail, ziet u deze niet controleer dan ook uw ongewenste
                            mail!
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<script src="https://unpkg.com/flowbite@1.4.3/dist/flowbite.js"></script>
</body>
</html>
