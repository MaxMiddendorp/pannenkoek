<div class="flex flex-row">
    <a href="{{ route('orders.full') }}" class="pr-4 pl-2"><i class="fa-solid fa-expand"></i></a>
    <p>Laatst bijgewerkt: {{ \Carbon\Carbon::now()->format('H:i:s') }}</p>
</div>
<table class="w-full text-sm text-left text-gray-500">
    <thead class="text-xs text-gray-700 uppercase bg-gray-50">
    <tr>
        <th scope="col" class="px-2 py-3">
            #
        </th>
        <th class="px-2 py-3">
            Tafel
        </th>
        <th scope="col" class="px-3 py-3">
            Bestelling
        </th>
        <th scope="col" class="px-6 py-3">
            Comment
        </th>
        <th scope="col" class="px-1 py-3">
            Status
        </th>
        <th scope="col" class="px-1 py-3">

        </th>
    </tr>
    </thead>
    <tbody>
    @foreach($orders as $order)
        <tr class="bg-white border-b">
            <th scope="row" class="px-2 py-4 font-medium text-gray-900 whitespace-nowrap">
                {{ $order->id }}
            </th>
            <td class="px-2 py-4">
                {{ $order->tafel }}
            </td>
            <td class="px-3 py-4">
                <table class="w-full text-sm text-left text-gray-500">
                    <tbody>
                    @foreach(unserialize($order->order) as $key => $item)
                        @if($item != 0)
                            @foreach($menuitems as $menuitem)
                                @if($key == $menuitem->id)
                                    <tr>
                                        <td class="w-4/5">{{ $menuitem->naam }}</td>
                                        <td class="w-1/5 text-right">{{ $item }}x</td>
                                        {{--                                        {{ $menuitem->naam . " x" . $item }} <br>--}}
                                    </tr>
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </td>
            <td class="px-6 py-4 break-all">
                {{ $order->message }}
            </td>
            <td class="px-1 py-4">
                @if($order->order_status == 0)
                    <span class="mr-3"><span
                            class="whitespace-nowrap text-uppercase inline-flex items-center px-2.5 py-1 text-sm rounded-full bg-red-400 bg-opacity-10 text-red-900"><span
                                class="relative flex mr-1.5 w-2.5 h-2.5"><span
                                    class="relative inline-flex w-2.5 h-2.5 rounded-full bg-red-400"></span></span>Besteld</span></span>
                @elseif($order->order_status == 1)
                    <span class="mr-3"><span
                            class="whitespace-nowrap text-uppercase inline-flex items-center px-2.5 py-1 text-sm rounded-full bg-amber-400 bg-opacity-10 text-amber-900"><span
                                class="relative flex mr-1.5 w-2.5 h-2.5"><span
                                    class="relative inline-flex w-2.5 h-2.5 rounded-full bg-amber-400"></span></span>Wordt bereid</span></span>
                @elseif($order->order_status == 2)
                    <span class="mr-3"><span
                            class="whitespace-nowrap text-uppercase inline-flex items-center px-2.5 py-1 text-sm rounded-full bg-lime-400 bg-opacity-10 text-lime-900"><span
                                class="relative flex mr-1.5 w-2.5 h-2.5"><span
                                    class="relative inline-flex w-2.5 h-2.5 rounded-full bg-lime-400"></span></span>Serveren</span></span>
                @elseif($order->order_status == 3)
                    <span class="mr-3"><span
                            class="whitespace-nowrap text-uppercase inline-flex items-center px-2.5 py-1 text-sm rounded-full bg-teal-400 bg-opacity-10 text-gray-900"><span
                                class="relative flex mr-1.5 w-2.5 h-2.5"><span
                                    class="relative inline-flex w-2.5 h-2.5 rounded-full bg-teal-400"></span></span>Klaar</span></span>
                @else
                    Error
                @endif
            </td>
            <td class="px-1 py-4 text-right flex flex-row">
                <form method="POST" action="{{ route('orders.status', [ 'order' => $order->id ]) }}">
                    @csrf

                    <a href="{{ route('orders.status', [ 'order' => $order->id ]) }}"
                       onclick="event.preventDefault();
                                                this.closest('form').submit();">
                        <i class="fa-solid fa-forward-fast"></i>
                    </a>
                </form>

                @if(\Illuminate\Support\Facades\Auth::user()->is_admin)
                    <a href="{{ route('orders.edit', [ 'order' => $order->id ]) }}"><i
                            class="fa-solid fa-pen ml-4"></i></a>
                @endif

                @if(\Illuminate\Support\Facades\Auth::user()->is_admin)
                    {{--                    <form method="POST"--}}
                    {{--                          onsubmit="return confirm('Weet je zeker dat je deze bestelling wilt verwijderen?');"--}}
                    {{--                          action="{{ route('orders.delete', [ 'order' => $order->id ]) }}">--}}
                    {{--                        @csrf--}}

                    {{--                        <a href="{{ route('orders.delete', [ 'order' => $order->id ]) }}"--}}
                    {{--                           onsubmit="return confirm('Weet je zeker dat je deze bestelling wilt verwijderen?');"--}}
                    {{--                           onclick="event.preventDefault();--}}
                    {{--                                                this.closest('form').submit();">--}}
                    {{--                            <i class="fa-solid fa-trash px-2"></i> </a>--}}
                    {{--                    </form>--}}
                    <a type="button"
                       onclick="return confirm('Weet je zeker dat je deze bestelling wilt verwijderen?', 'Let op!')"
                       href="{{ route('orders.delete', ['order' => $order->id]) }}">
                        <i class="fa-solid fa-trash px-2"></i>
                    </a>
                @endif

            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<p class="pt-4">Resulaten gelimiteerd tot 30</p>
