<x-app-layout>
    <div class="p-2 bg-white border-b border-gray-200">
        <div id="reload" class="overflow-x-auto"></div>
    </div>
    <script>
        var count = 0;

        function loadlink() {
            $('#reload').load('/orders/listfull');
            // console.log('Refreshed data');
            count++;
            // var count = $i++;
            console.log('Page has been refreshed ' + count + ' times');
        }

        loadlink();
        setInterval(function () {
            loadlink()
        }, 5000);
        // }, 50000);
    </script>
</x-app-layout>
