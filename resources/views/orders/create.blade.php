<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Bestelling maken') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="POST" action="{{ route('orders.save') }}">
                        @csrf
                        <label class="block">
                            <span class="text-gray-700">Tafel</span>
                            <select name="tafel"
                                    class="form-select appearance-none block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none">
                                <option selected disabled>Selecteer tafel</option>
                                @foreach($tables as $table)
                                    <option value="{{ $table->tablenumber }}">Tafel {{ $table->tablenumber }}</option>
                                @endforeach
                            </select>
                        </label>
                        <hr class="my-6">
                        <div class="w-full flex flex-col lg:flex-row lg:flex-wrap">
                            <p class="">Pannenkoeken</p><br><br>
                            <div class="w-full flex flex-col lg:flex-row lg:flex-wrap pb-4 border-b">
                                {{--                            Pannenkoeken --}}
                                @foreach($menu_pancakes as $pannenkoeken)
                                    <label class="block w-full lg:w-1/4 mt-2">
                                        <div class="w-full flex">
                                            <span class="text-gray-700 ml-2">{{ $pannenkoeken->naam }} </span>
                                            <span
                                                class="text-gray-700 ml-auto mr-2">{{ $pannenkoeken->strippen }} </span>
                                        </div>
                                        {{--                                    <input type="number" name="order[{{ $menuitem->id }}]" value="0"--}}
                                        {{--                                           class=" mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 "--}}
                                        {{--                                           placeholder="">--}}
                                        <select name="order[{{ $pannenkoeken->id }}]"
                                                class="form-select appearance-none block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none">
                                            <option selected value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                        </select>
                                    </label>
                                @endforeach
                            </div>
                            <p class="py-4">Pannenkoeken 18+</p>
                            <div class="w-full flex flex-col lg:flex-row lg:flex-wrap pb-4 border-b">
                                {{--                            Pannenkoeken 18+ --}}
                                @foreach($menu_pancakes_18 as $pannenkoeken18)
                                    <label class="block w-full lg:w-1/4 mt-2">
                                        <div class="w-full flex">
                                            <span class="text-gray-700 ml-2">{{ $pannenkoeken18->naam }} </span>
                                            <span
                                                class="text-gray-700 ml-auto mr-2">{{ $pannenkoeken18->strippen }} </span>
                                        </div>
                                        {{--                                    <input type="number" name="order[{{ $menuitem->id }}]" value="0"--}}
                                        {{--                                           class=" mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 "--}}
                                        {{--                                           placeholder="">--}}
                                        <select name="order[{{ $pannenkoeken18->id }}]"
                                                class="form-select appearance-none block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none">
                                            <option selected value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                        </select>
                                    </label>
                                @endforeach
                            </div>
                            <p class="py-4">Drinken</p>
                            <div class="w-full flex flex-col lg:flex-row lg:flex-wrap pb-4 border-b">
                                {{--                            Drinken --}}
                                @foreach($menu_drinks as $drinken)
                                    <label class="block w-full lg:w-1/4 mt-2">
                                        <div class="w-full flex">
                                            <span class="text-gray-700 ml-2">{{ $drinken->naam }} </span>
                                            <span class="text-gray-700 ml-auto mr-2">{{ $drinken->strippen }} </span>
                                        </div>
                                        {{--                                    <input type="number" name="order[{{ $menuitem->id }}]" value="0"--}}
                                        {{--                                           class=" mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 "--}}
                                        {{--                                           placeholder="">--}}
                                        <select name="order[{{ $drinken->id }}]"
                                                class="form-select appearance-none block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none">
                                            <option selected value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                        </select>
                                    </label>
                                @endforeach
                            </div>
                            <p class="py-4">Drinken 18+</p>
                            <div class="w-full flex flex-col lg:flex-row lg:flex-wrap pb-4">
                                {{--                            Drinken 18+ --}}
                                @foreach($menu_drinks_18 as $drinken18)
                                    <label class="block w-full lg:w-1/4 mt-2">
                                        <div class="w-full flex">
                                            <span class="text-gray-700 ml-2">{{ $drinken18->naam }} </span>
                                            <span class="text-gray-700 ml-auto mr-2">{{ $drinken18->strippen }} </span>
                                        </div>
                                        {{--                                    <input type="number" name="order[{{ $menuitem->id }}]" value="0"--}}
                                        {{--                                           class=" mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 "--}}
                                        {{--                                           placeholder="">--}}
                                        <select name="order[{{ $drinken18->id }}]"
                                                class="form-select appearance-none block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none">
                                            <option selected value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                        </select>
                                    </label>
                                @endforeach
                            </div>
                        </div>
                        <hr class="my-6">

                        <label class="block">
                            <span class="text-gray-700">Opmerking</span>
                            <textarea name="message"
                                      class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"></textarea>
                        </label>
                        <button
                            class="mt-4 block text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                            type="button" data-modal-toggle="popup-modal">
                            Volgende
                        </button>
                        <div id="popup-modal" tabindex="-1"
                             class="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 md:inset-0 h-modal md:h-full">
                            <div class="relative p-4 w-full max-w-md h-full md:h-auto">
                                <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
                                    <div class="p-6 text-center">
                                        {{--                                        <svg class="mx-auto mb-4 w-14 h-14 text-gray-400 dark:text-gray-200" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>--}}
                                        <i class="mx-auto mb-4 text-gray-400 dark:text-gray-200 fa-solid fa-receipt fa-2xl"></i>
                                        {{--                                        TODO: Insert/calculate correct amount --}}
                                        <h3 class="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400 mt-4">
                                            Vergeet niet om de consumptiebonnen te nemen</h3>
                                        <svg class="h-12 pb-4 ml-auto mr-auto" xmlns="http://www.w3.org/2000/svg"
                                             viewBox="0 0 398.072 107.527">
                                            <path
                                                d="M117.074 0c-8.475 0-15.111 5.588-15.111 12.724v81.841c0 7.138 6.638 12.726 15.111 12.726 8.437 0 15.299-5.709 15.299-12.726V12.723C132.373 5.708 125.511 0 117.074 0zM68.906.656c-8.451 0-15.072 5.571-15.072 12.686v31.682L28.016 6.742l-.162-.221c-.144-.192-.285-.381-.456-.571C24.91 2.61 20.781.695 16.065.695c-.154 0-.309.003-.442.009-.063 0-.125-.012-.188-.019a2.881 2.881 0 0 0-.37-.029C6.617.656 0 6.228 0 13.341v81.516c0 7.105 6.617 12.67 15.064 12.67 8.397 0 15.229-5.682 15.229-12.67V62.991l24.548 36.376c.838 1.877 2.196 3.571 3.906 4.883 2.485 2.098 5.799 3.252 9.326 3.252.123 0 .245-.002.354-.004.056 0 .109.008.162.012.103.01.206.018.315.018 8.405 0 15.244-5.682 15.244-12.67V13.341C84.15 6.347 77.312.656 68.906.656z"
                                                fill="#ee712a"/>
                                            <path
                                                d="M288.373 1.24c-2.343 0-4.642.421-6.814 1.238l-17.837 6.411c-7.713 2.744-11.281 11.868-8.121 20.778 2.505 6.979 8.781 11.854 15.259 11.854h.002c.22 0 .439-.006.656-.016v50.586c0 8.195 7.404 14.615 16.855 14.615 9.379 0 17.01-6.558 17.01-14.615V15.862c0-8.063-7.631-14.622-17.01-14.622zm0 98.556c-5.578 0-9.947-3.385-9.947-7.705V32.192l-5.66 2.085a5.635 5.635 0 0 1-1.871.313c-3.588 0-7.285-3.045-8.789-7.234-1.858-5.261-.129-10.513 3.945-11.959l17.899-6.436a12.596 12.596 0 0 1 4.423-.812c5.568 0 10.1 3.459 10.1 7.712V92.09c0 4.249-4.531 7.706-10.1 7.706zM387.69 47.678c3.318-4.865 5.064-10.381 5.064-16.028 0-16.769-14.758-30.41-32.898-30.41-18.131 0-32.883 13.643-32.883 30.41 0 5.654 1.742 11.171 5.051 16.036-6.613 6.527-10.38 15.205-10.38 24.002 0 19.463 17.142 35.295 38.212 35.295 21.072 0 38.217-15.832 38.217-35.295 0-8.801-3.77-17.481-10.383-24.01zm-27.834 52.395c-17.262 0-31.305-12.731-31.305-28.385 0-7.85 3.768-15.471 10.336-20.909l2.699-2.226-2.256-2.663c-3.564-4.215-5.447-9.14-5.447-14.241 0-12.958 11.649-23.5 25.973-23.5 14.332 0 25.99 10.542 25.99 23.5 0 5.088-1.889 10.013-5.465 14.243l-2.254 2.674 2.698 2.212c6.664 5.505 10.337 12.933 10.337 20.909.001 15.654-14.042 28.386-31.306 28.386z"
                                                fill="#fab400"/>
                                            <path
                                                d="M183.221 62.245c-.924-.804-3.571-3.457-3.741-7.62-.089-2.342.593-5.777 4.338-8.908l7.949-8.311 5.293-5.705-21.779-23.293c-3.464-4.231-7.558-6.469-11.835-6.469-3.731 0-7.511 1.645-11.272 4.929-7.604 7.79-4.894 16.071.165 21.213l24.457 26.3-24.582 26.646c-3.726 4.164-7.733 13.355.873 21.41 2.874 2.615 6.111 3.939 9.623 3.939 6.525 0 11.427-4.646 12.356-5.592l21.908-23.679-13.753-14.86z"
                                                fill="#ee712a"/>
                                            <path
                                                d="M243.199 6.244l-.086-.082c-3.687-3.209-7.687-4.906-11.565-4.906-4.551 0-8.812 2.268-12.326 6.557l-27.455 29.596-7.949 8.31c-3.746 3.131-4.427 6.566-4.338 8.908.17 4.163 2.817 6.816 3.741 7.62l36.201 39.117c.942.962 5.992 5.752 12.832 5.752 3.694 0 7.104-1.397 10.149-4.17 9.011-8.422 4.787-18.067.855-22.457l-24.085-26.104 23.915-25.723c5.342-5.423 8.178-14.159.111-22.418zm-32.1 48.113l27.775 30.111c3.228 3.594 4.792 9.195-.482 14.121-1.896 1.729-3.961 2.604-6.139 2.604-3.24 0-6.64-1.979-8.539-3.907l-36.367-39.28-.125-.121c-.523-.447-1.752-1.682-1.824-3.502-.041-.941.168-2.416 2.435-4.322l35.839-38.348.062-.07c2.457-2.994 5.101-4.511 7.854-4.511 2.383 0 4.897 1.124 7.481 3.343 4.354 4.497 4.271 9.492-.262 14.104l-27.708 29.778z"
                                                fill="#fab400"/>
                                        </svg>
                                        <button data-modal-toggle="popup-modal" type="submit"
                                                class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                            Bestelling plaatsen
                                        </button>
                                        <button data-modal-toggle="popup-modal" type="button"
                                                class="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-gray-200 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600">
                                            Annuleren
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--                        <button type="submit"--}}
                        {{--                                class="inline-block px-6 mt-8 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out">--}}
                        {{--                            Button--}}
                        {{--                        </button>--}}
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
