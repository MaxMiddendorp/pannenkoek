<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Bestelling maken') }}
        </h2>
    </x-slot>

    <div class="py-12">

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="POST" action="{{ route('orders.editsave', [ 'order' => $order ]) }}">
                        @csrf
{{--                        @dd($bestelling)--}}
                        <label class="block">
                            <span class="text-gray-700">Tafel</span>
                            <select name="tafel"
                                    class="form-select appearance-none block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none">
                                <option selected disabled>Selecteer tafel</option>
                                @foreach($tables as $table)
                                    <option @if($order->tafel == $table->tablenumber) selected @endif value="{{ $table->tablenumber }}">Tafel {{ $table->tablenumber }}</option>
                                @endforeach
                            </select>
                        </label>
                        <hr class="my-6">
{{--                        <div class="w-full flex flex-col lg:flex-row lg:flex-wrap">--}}
{{--                            @foreach($menuitems as $menuitem)--}}
{{--                                <label class="block w-full lg:w-1/4 mt-2">--}}
{{--                                    <span class="text-gray-700">{{ $menuitem->naam }} </span>--}}
{{--                                    <span class="text-gray-700 w-full">{{ $menuitem->strippen }} </span>--}}
{{--                                    --}}{{--                                    <input type="number" name="order[{{ $menuitem->id }}]" value="0"--}}
{{--                                    --}}{{--                                           class=" mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 "--}}
{{--                                    --}}{{--                                           placeholder="">--}}
{{--                                    <select name="order[{{ $menuitem->id }}]"--}}
{{--                                            class="form-select appearance-none block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none">--}}
{{--                                        <option @if($bestelling->id == $menuitem->id) selected='selected' @endif value="0">0</option>--}}
{{--                                        <option @if($bestelling->id == $menuitem->id) selected='selected' @endif value="1">1</option>--}}
{{--                                        <option @if($bestelling->id == $menuitem->id) selected='selected' @endif value="2">2</option>--}}
{{--                                        <option @if($bestelling->id == $menuitem->id) selected='selected' @endif value="3">3</option>--}}
{{--                                        <option @if($bestelling->id == $menuitem->id) selected='selected' @endif value="4">4</option>--}}
{{--                                        <option value="5">5</option>--}}
{{--                                        <option value="6">6</option>--}}
{{--                                        <option value="7">7</option>--}}
{{--                                        <option value="8">8</option>--}}
{{--                                        <option value="9">9</option>--}}
{{--                                        <option value="10">10</option>--}}
{{--                                    </select>--}}
{{--                                </label>--}}
{{--                            @endforeach--}}
{{--                        </div>--}}
                        Changing order is currently not available
                        <hr class="my-6">

                        <label class="block">
                            <span class="text-gray-700">Opmerking</span>
{{--                            @dd($order->message)--}}
                            <textarea name="message" class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">{{ $order->message }}</textarea>
                        </label>
                        <button
                            class="mt-4 block text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                            type="button" data-modal-toggle="popup-modal">
                            Volgende
                        </button>
                        <div id="popup-modal" tabindex="-1"
                             class="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 md:inset-0 h-modal md:h-full">
                            <div class="relative p-4 w-full max-w-md h-full md:h-auto">
                                <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
                                    <div class="p-6 text-center">
                                        {{--                                        <svg class="mx-auto mb-4 w-14 h-14 text-gray-400 dark:text-gray-200" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>--}}
                                        <i class="mx-auto mb-4 text-gray-400 dark:text-gray-200 fa-solid fa-receipt fa-2xl"></i>
                                        {{--                                        TODO: Insert/calculate correct amount --}}
                                        <h3 class="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400 mt-4">
                                            Neem</h3>
                                        <h3 class="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400 font-bold">
                                            5</h3>
                                        <h3 class="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400">
                                            Consumptiebonnen</h3>
                                        <p>Note: the amount shown is not correct as this is a placeholder</p>
                                        <button data-modal-toggle="popup-modal" type="submit"
                                                class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                            Bestelling plaatsen
                                        </button>
                                        <button data-modal-toggle="popup-modal" type="button"
                                                class="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-gray-200 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600">
                                            Annuleren
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--                        <button type="submit"--}}
                        {{--                                class="inline-block px-6 mt-8 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out">--}}
                        {{--                            Button--}}
                        {{--                        </button>--}}
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
