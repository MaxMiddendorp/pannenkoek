<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Bestellingen') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div id="reload" class="overflow-x-auto"></div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var count = 0;
        function loadlink() {
            $('#reload').load('/orders/list');
            // console.log('Refreshed data');
            count++;
            // var count = $i++;
            console.log('Page has been refreshed ' + count + ' times');
        }

        loadlink();
        setInterval(function () {
            loadlink()
        }, 5000);
        // }, 50000);
    </script>
</x-app-layout>
