<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Menuitems') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <a href="{{ route('menu.create') }}"><i
                            class="fa-solid fa-plus fill-gray-500 p-3 object-right	"></i></a>
                    <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
                        <table class="w-full text-sm text-left text-gray-500">
                            <thead class="text-xs text-gray-700 uppercase bg-gray-50">
                            <tr>
                                <th scope="col" class="px-6 py-3">
                                    #
                                </th>
                                <th class="px-6 py-3">
                                    Naam
                                </th>
                                <th class="px-6 py-3">

                                </th>
                                <th class="px-6 py-3">

                                </th>
                                <th class="px-6 py-3">

                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Strippen
                                </th>
                                <th scope="col" class="px-6 py-3">

                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($menus as $menu)
                                <tr class="bg-white border-b">
                                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                                        {{ $menu->id }}
                                    </th>
                                    <td class="px-6 py-4">
                                        {{ $menu->naam }}
                                    </td>
                                    <td class="px-6 py-4 border w-8">
                                        @if($menu->pannenkoek == 1)
                                            🥞
                                        @endif
                                    </td>
                                    <td class="px-6 py-4 border w-8">
                                        @if($menu->drinken == 1)
                                            🥛
                                        @endif
                                    </td>
                                    <td class="px-6 py-4 border w-8">
                                        @if($menu->achttien == 1)
                                            🔞
                                        @endif
                                    </td>
                                    <td class="px-6 py-4">
                                        @if($menu->strippen != null)
                                            {{ $menu->strippen }}
                                        @endif
                                    </td>
                                    <td class="px-6 py-4 text-right flex flex-row">
                                        <a href="{{ route('menu.edit', [ 'menu' => $menu->id ]) }}"><i
                                                class="fa-solid fa-pen px-2"></i></a>
                                        {{--                                        <a href="{{ route('users.delete', [ 'user' => $menu->id ]) }}"><i--}}
                                        {{--                                                class="fa-solid fa-trash"></i></a>--}}
                                        <form method="POST"
                                              action="{{ route('menu.delete', [ 'menu' => $menu->id ]) }}">
                                            @csrf

                                            <a href="{{ route('menu.delete', [ 'menu' => $menu->id ]) }}"
                                               onclick="event.preventDefault();
                                                this.closest('form').submit();">
                                                <i class="fa-solid fa-trash px-2"></i> </a>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
