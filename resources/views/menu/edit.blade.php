<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Menuitem bewerken') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="POST" action="{{ route('menu.editsave', [ 'menu' => $menu ]) }}">
                        @csrf
                        <label class="block">
                            <span class="text-gray-700">Naam</span>
                            <input type="text" name="naam" value="{{$menu->naam}}"
                                   class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                                   placeholder="">
                        </label>
                        <div class="flex items-center mb-4 my-4">
                            <input id="default-checkbox" name="pannenkoek" type="checkbox" value="1" @if($menu->pannenkoek == 1) checked @endif
                                   class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="default-checkbox"
                                   class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Pannenkoek</label>
                        </div>
                        <div class="flex items-center mb-4 my-4">
                            <input id="drinken" name="drinken" type="checkbox" value="1" @if($menu->drinken == 1) checked @endif
                                   class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="drinken"
                                   class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Drinken</label>
                        </div>
                        <div class="flex items-center mb-4">
                            <input id="default-checkbox" name="achttien" type="checkbox" value="1" @if($menu->achttien == 1) checked @endif
                                   class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="default-checkbox"
                                   class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">18+</label>
                        </div>
                        <label class="block">
                            <span class="text-gray-700">Aantal strippen</span>
                            <input type="number" name="strippen" value="{{$menu->strippen}}"
                                   class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
                        </label>
                        <button
                            class="inline-block px-6 mt-8 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out"
                            type="submit">Opslaan
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
</x-app-layout>
