<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    Welkom, {{ Auth::User()->name }}
                    <br>
                        <table>
                            <tr>
                                <th colspan="4">Statestieken</th>
                            </tr>
                            <tr>
                                <th class="px-1.5 md:px-4">Besteld</th>
                                <th class="px-1.5 md:px-4">Wordt bereid</th>
                                <th class="px-1.5 md:px-4">Serveren</th>
                                <th class="px-1.5 md:px-4">Afgerond</th>
                            </tr>
                            <tr>
                                <td class="text-center">{{ $status0 }}</td>
                                <td class="text-center">{{ $status1 }}</td>
                                <td class="text-center">{{ $status2 }}</td>
                                <td class="text-center">{{ $status3 }}</td>
                            </tr>
                        </table>

                    {{--                    <img src="https://via.placeholder.com/900x600?text=Placeholder:+Plattegrond+buiten+of+binnen"--}}
                    {{--                         alt="Placeholder plattegrond"/>--}}

                    {{--                    <img src="{{ asset('/img/maps/Pannenkoekenrestaurant-buiten.png') }}"--}}
                    {{--                         alt="Placeholder plattegrond" class="w-2/3"/>--}}
                    <img src="{{ asset('/img/maps/IMG_20220514_011436 (Large).jpg') }}"
                         alt="Placeholder plattegrond" class="w-full overflow-auto"/>

                    {{--                    <br>--}}
                    {{--                    <br>--}}

                    {{--                    <div id="map" style="background: #888888; height: 20rem;" class="w-full h-96"></div>--}}

                    {{--                    <script>--}}
                    {{--                        var map = L.map('map').setView([0, 0], 0);--}}
                    {{--                        L.tileLayer('{{ env('APP_URL') }}/img/maps/outside/{z}/{x}/{y}.png', { noWrap: true}).addTo(map);--}}
                    {{--                    </script>--}}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
