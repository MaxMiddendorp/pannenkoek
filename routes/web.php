<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ReservationController;
use App\Http\Controllers\TableController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::domain('www.' . env('APP_URL'))->middleware(Spatie\Csp\AddCspHeaders::class)->group(function () {
    app('App\Http\Controllers\HomeController')->index();
});

Route::domain(env('APP_URL'))->middleware(Spatie\Csp\AddCspHeaders::class)->group(function () {
    Route::get('/', [HomeController::class, 'index'])->name('home.index');
    Route::post('/', [HomeController::class, 'save'])->name('home.save');
    Route::get('/done', [HomeController::class, 'done'])->name('home.done');
    Route::get('/mail', [HomeController::class, 'showmail'])->name('home.showmail');
});

Route::domain('admin.' . env('APP_URL'))->middleware(Spatie\Csp\AddCspHeaders::class)->group(function () {
    Route::get('/', function () {
        return redirect('login');
    });

    Route::middleware(['auth'])->group(function () {
//        Route::get('/dashboard', function () {
//            return view('dashboard');
//        })->name('dashboard');
        Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');


        Route::get('/orders', [OrderController::class, 'index'])->name('orders.index');
        Route::get('/orders/list', [OrderController::class, 'list'])->name('orders.list');
        Route::get('/orders/listfull', [OrderController::class, 'listfull'])->name('orders.listfull');
        Route::get('/orders/full', [OrderController::class, 'full'])->name('orders.full');
        Route::get('/orders/create', [OrderController::class, 'create'])->name('orders.create');
        Route::post('/orders/create', [OrderController::class, 'save'])->name('orders.save');
        Route::get('/orders/edit/{order}', [OrderController::class, 'edit'])->name('orders.edit');
        Route::post('/orders/edit/{order}', [OrderController::class, 'editsave'])->name('orders.editsave');
//    Route::post('/orders/delete/{order}', [OrderController::class, 'delete'])->name('orders.delete');
        Route::post('/orders/status/{order}', [OrderController::class, 'status'])->name('orders.status');
        Route::get('/orders/delete/{order}', [OrderController::class, 'delete'])->name('orders.delete');

        Route::get('/reservations', [ReservationController::class, 'index'])->name('reservations.index');
        Route::get('/reservations/create', [ReservationController::class, 'create'])->name('reservations.create');
        Route::post('/reservations/create', [ReservationController::class, 'save'])->name('reservations.save');
        Route::get('/reservations/edit/{reservation}', [ReservationController::class, 'edit'])->name('reservations.edit');
        Route::post('/reservations/edit/{reservation}', [ReservationController::class, 'editsave'])->name('reservations.editsave');
        Route::post('/reservations/delete/{reservation}', [ReservationController::class, 'delete'])->name('reservations.delete');
        Route::post('/reservations/status/{reservation}', [ReservationController::class, 'status'])->name('reservations.status');

        Route::get('/tables', [TableController::class, 'index'])->name('tables.index');
        Route::get('/tables/create', [TableController::class, 'create'])->name('tables.create');
        Route::post('/tables/create', [TableController::class, 'save'])->name('tables.save');
//      Route::get('/tables/edit/{table}', [TableController::class, 'edit'])->name('tables.edit');
//      Route::post('/tables/edit/{table}', [TableController::class, 'editsave'])->name('tables.editsave');
//      Route::post('/tables/delete/{table}', [TableController::class, 'delete'])->name('tables.delete');
        Route::post('/tables/status/{table}', [TableController::class, 'status'])->name('tables.status');

//    Admins only :)
        Route::middleware(['admin'])->group(function () {
            Route::get('/users', [UserController::class, 'index'])->name('users.index');
            Route::get('/users/create', [UserController::class, 'create'])->name('users.create');
            Route::post('/users/create', [UserController::class, 'save'])->name('users.save');
            Route::get('/users/edit/{user}', [UserController::class, 'edit'])->name('users.edit');
            Route::post('/users/edit/{user}', [UserController::class, 'editsave'])->name('users.editsave');
            Route::post('/users/delete/{user}', [UserController::class, 'delete'])->name('users.delete');

            Route::get('/menu', [MenuController::class, 'index'])->name('menu.index');
            Route::get('/menu/create', [MenuController::class, 'create'])->name('menu.create');
            Route::post('/menu/create', [MenuController::class, 'save'])->name('menu.save');
            Route::get('/menu/edit/{menu}', [MenuController::class, 'edit'])->name('menu.edit');
            Route::post('/menu/edit/{menu}', [MenuController::class, 'editsave'])->name('menu.editsave');
            Route::post('/menu/delete/{menu}', [MenuController::class, 'delete'])->name('menu.delete');
        });
    });
});

require __DIR__ . '/auth.php';
