<?php

namespace Database\Seeders;

use App\Models\Order;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 100; $i++) {
            $order = Order::create([
                'tafel' => '1',
                'order' => 'a:3:{i:1;s:1:"2";i:2;s:1:"1";i:3;s:1:"1";}',
                'order_status' => '0',
                'message' => '1234567890-=qwertyuiopasdfghjklzxcvbnm[]\;,./`/*-+958256845469/785489/546789/5478/rysfhhoyfihrygw7f6rvh83w64vyb4o36qrv76bo34baw893w6abvos6rovb6o94rvyest8yvbo7es8pv94esby7o9tgsvyt4o9aesvbyots9',
                'user_id' => 1,
            ]);
        }
    }
}
